import seeder from '@cleverbeagle/seeder';
import { Meteor } from 'meteor/meteor';
import Documents from '../../api/Documents/Documents';

const documentsSeed = userId => ({
  collection: Documents,
  environments: ['development', 'staging'],
  noLimit: true,
  modelCount: 5,
  model(dataIndex) {
    return {
      owner: userId,
      title: `Document #${dataIndex + 1}`,
      body: `This is the body of document #${dataIndex + 1}`,
    };
  },
});

seeder(Meteor.users, {
  environments: ['development', 'staging'],
  noLimit: true,
  data: [{
    email: 'admin@admin.com',
    password: 'password',
    profile: {
      username: 'andywarhol',
      fullname: 'Andy Warhol',
    },
    accounts: {
      steemAccount: {
        username: 'nameless-berk',
      },
      golosAccount: {
        username: 'nameless-berk',
      },
    },
    roles: ['admin'],
    data(userId) {
      return documentsSeed(userId);
    },
  }],
  modelCount: 5,
  model(index, faker) {
    const userCount = index + 1;
    let firstName = faker.name.firstName();
    let lastName  = faker.name.lastName();
    return {
      email: `user+${userCount}@test.com`,
      password: 'password',
      profile: {
        username: firstName.toLowerCase()+lastName.toLowerCase(),
        fullname: '${firstName} ${lastName}',
      },
      accounts: {
        steemAccount: '',
        golosAccount: '',
      },
      roles: ['user'],
      data(userId) {
        return documentsSeed(userId);
      },
    };
  },
});
