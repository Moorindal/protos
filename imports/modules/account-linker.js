import { Meteor } from 'meteor/meteor';

if (Meteor.isClient) {
  import $ from 'jquery';
  import 'jquery-validation';
  import sc2 from 'sc2-sdk';
  import qs from 'qs';
  import LinkedAccountsCollection from '../api/LinkedAccounts/LinkedAccounts';
  import { Bert } from 'meteor/themeteorchef:bert';
  
  export const getSteemConnectLink = () => {
    let api = sc2.Initialize({
      app: 'protos.app',
      callbackURL: 'http://localhost:3000/settings',
      scope: ['vote', 'comment']
    });

    let link = api.getLoginURL('state')
    console.log(link)
    
    window.location.assign(link)
  }
  
  export const linkSteemAccount = (props) => {
    console.log('linking STEEM: ');
    var parsed = qs.parse(props.location.search);
    var access_token = qs.parse(props.location.search).access_token ? 
                       qs.parse(props.location.search).access_token : 
                       qs.parse(props.location.search)['?access_token'];
              
    var expires_in = qs.parse(props.location.search).expires_in;
    var username = qs.parse(props.location.search).username;
    
    let account = {
      owner: props.userId,
      network: 'STEEM',
      username: username,
      data : {
        accessToken: access_token,
        expiresIn: expires_in,
      },
    };
    
    console.log('Sending to secure storage ', account);
    Meteor.call('linkedAccounts.connectSteem' , account, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Account linked!', 'success');
      }
    });
    
    let accounts = (props.user && props.user.accounts) ? props.user.accounts : {};
    accounts.steem = username;
    
    console.log('Sending to public storage ', accounts);
    Meteor.call('users.connectSteem', accounts, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Accounts updated!', 'success');
      }
    });
  }
}
