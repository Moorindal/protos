import { Meteor } from 'meteor/meteor';

if (Meteor.isClient) {
  import $ from 'jquery';
  import 'jquery-validation';
  import { Bert } from 'meteor/themeteorchef:bert';
  
  let action;
  
  const buildUserFeed = ({username, accounts }, promise) => {
    console.log('buildFeed invoked for '+username);
    console.log('accounts involved: ', accounts);
    if (!accounts || !accounts.steem) return false;
        
    const {Client} = require('dsteem')
    const client = new Client('https://api.steemit.com')

    var query = {
      tag: username, // This tag is used to filter the results by a specific post tag
      limit: 10, // This limit allows us to limit the overall results returned to 5
    }

    var result = {}
    action = promise
    
    client.database
    .getDiscussions('blog', query)
    .then(res => {
      console.log('server said, ',res)
      result = res
      action.resolve(res);
//       return result;
//             this.setState({ feedData: result })
    })
    .catch(err => {
      action.reject(err.message);
    })
    
//     console.log('output: ', result);
//     return result;
  }
  
export default options =>
  new Promise((resolve, reject) =>
    buildUserFeed(options, { resolve, reject }));
  
}
