export default (state = {}, { type, ...rest }) => {
  switch (type) {
    case 'ON_LOGIN':
      return { ...state, ...rest };
    case 'ON_LOGOUT':
      return { ...state, ...rest };
    case 'SET_FEED':
      return { ...state, ...rest };
    case 'TEST_ACTION':
      console.log('reducer');
      return { ...state, ...rest };
    default:
      return state;
  }
};
