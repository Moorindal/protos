import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';

export function onLogin() {
  return (dispatch) => {
    const loggingIn = Meteor.loggingIn();
    const user = Meteor.user();
    const userId = Meteor.userId();
    const username = user && user.profile && user.profile.username && user.profile.username;
    const fullname = user && user.profile && user.profile.fullname && user.profile.fullname;
    const emailAddress = user && user.emails && user.emails[0].address;

    dispatch({
      type: 'ON_LOGIN',
      loading: false,
      loggingIn,
      authenticated: !loggingIn && !!userId,
      username: username,
      fullname: fullname || emailAddress,
      roles: Roles.getRolesForUser(userId),
      userId,
      emailAddress,
      emailVerified: user && user.emails ? user && user.emails && user.emails[0].verified : true,
    });
  };
}

export function onLogout() {
  return (dispatch) => {
    dispatch({
      type: 'ON_LOGOUT',
      loading: false,
      loggingIn: false,
      authenticated: false,
      username: '',
      fullname: '',
      roles: [],
      userId: null,
      emailAddress: '',
      emailVerified: false,
    });
  };
}

export function setFeed(feed) {
  console.log('action ', feed)
  return (dispatch) => {
    dispatch({
      type: 'SET_FEED',
      feed: feed,
    });
  };
}

export function testAction() {
  // Thunk middleware knows how to handle functions.
  // It passes the dispatch method as an argument to the function,
  // thus making it able to dispatch actions itself.
  return (dispatch) => {
    console.log('action');
    setTimeout(() => {
      console.log('action 2');  
      dispatch({
        type: 'TEST_ACTION',
        testProp: 'Value',
      });
    }, 5000)
    // First dispatch: the app state is updated to inform
    // that the API call is starting.

    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.
  };
}
