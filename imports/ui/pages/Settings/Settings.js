/* eslint-disable no-underscore-dangle */

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import FileSaver from 'file-saver';
import base64ToBlob from 'b64-to-blob';
import { Grid, Row, Col, FormGroup, ControlLabel, Button, Tabs, Tab } from 'react-bootstrap';
import _ from 'lodash';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Bert } from 'meteor/themeteorchef:bert';
import { withTracker } from 'meteor/react-meteor-data';
import InputHint from '../../components/InputHint/InputHint';
import AccountPageFooter from '../../components/AccountPageFooter/AccountPageFooter';
import UserSettings from '../../components/UserSettings/UserSettings';
import validate from '../../../modules/validate';
import getUserProfile from '../../../modules/get-user-profile';
import { connect } from 'react-redux';
import { compose } from 'redux';
import * as actions from '../../../modules/redux/actions';
import AccountLinker from '../../../modules/account-linker';

const StyledProfile = styled.div`
  .nav.nav-tabs {
    margin-bottom: 20px;
  }
  
  .LoggedInWith {
    padding: 20px;
    border-radius: 3px;
    color: #fff;
    border: 1px solid var(--gray-lighter);
    text-align: center;

    img {
      width: 100px;
    }

    &.github img {
      width: 125px;
    }

    p {
      margin: 20px 0 0 0;
      color: var(--gray-light);
    }

    .btn {
      margin-top: 20px;

      &.btn-facebook {
        background: var(--facebook);
        border-color: var(--facebook);
        color: #fff;
      }

      &.btn-google {
        background: var(--google);
        border-color: var(--google);
        color: #fff;
      }

      &.btn-github {
        background: var(--github);
        border-color: var(--github);
        color: #fff;
      }
    }
  }

  .btn-export {
    padding: 0;
  }
`;

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 'profile' };
    autoBind(this);
    
    console.log('this');
    console.dir(this);
    
    console.log('props');
    console.dir(props);
    
//     this.props.dispatch(actions.testAction());
    
//     props.handleTestAction();
//     let testState = 
//     console.log('store');
//     console.dir(store);
  }

  componentDidMount(props) {
    const component = this;

    validate(component.form, {
      rules: {
        username: {
          required: true,
        },
        fullname: {
          required: false,
        },
        emailAddress: {
          required: true,
          email: true,
        },
        currentPassword: {
          required() {
            // Only required if newPassword field has a value.
            return component.form.newPassword.value.length > 0;
          },
        },
        newPassword: {
          required() {
            // Only required if currentPassword field has a value.
            return component.form.currentPassword.value.length > 0;
          },
          minlength: 6,
        },
      },
      messages: {
        username: {
          required: 'What\'s your username?',
        },
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?',
        },
        currentPassword: {
          required: 'Need your current password if changing.',
        },
        newPassword: {
          required: 'Need your new password if changing.',
        },
      },
      submitHandler() { component.handleSubmit(component.form); },
    });
    
      if (this.props.location.search) {
        console.log('Props ', this.props);
        AccountLinker.linkSteemAccount(this.props);
      }
      
  }

  getUserType(user) {
    return user.service === 'password' ? 'password' : 'oauth';
  }

  handleExportData(event) {
    event.preventDefault();
    Meteor.call('users.exportData', (error, exportData) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        FileSaver.saveAs(base64ToBlob(exportData), `${Meteor.userId()}.zip`);
      }
    });
  }

  handleDeleteAccount() {
    if (confirm('Are you sure? This will permanently delete your account and all of its data.')) {
      Meteor.call('users.deleteAccount', (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Account deleted!', 'success');
        }
      });
    }
  }

  handleSubmit(form) {
    const profile = {
      emailAddress: form.emailAddress.value,
      profile: {
        username: form.username.value,
        fullname: form.fullname.value,
      },
//       accounts: {
//         steem: form.steemAccount.value,
//         golos: form.golosAccount.value,
//       },
    };
    
    console.log('Sending profile:');
    console.dir(profile);
    

    Meteor.call('users.editProfile', profile, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Profile updated!', 'success');
      }
    });

    if (form.newPassword.value) {
      Accounts.changePassword(form.currentPassword.value, form.newPassword.value, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          form.currentPassword.value = ''; // eslint-disable-line no-param-reassign
          form.newPassword.value = ''; // eslint-disable-line no-param-reassign
        }
      });
    }
  }
  
  handleLinkSteem() {
//     console.log('getting the link');
    AccountLinker.getSteemConnectLink();
  }
  
  handleSubscriptionTest() {
    console.log('handleSubscriptionTest');
//     console.dir(this.props.linkedAccsSub);
//     console.dir(LinkedAccountsCollection);
//     
//     console.log('Find all test:');
//     let res = LinkedAccountsCollection.find().fetch();
//     
//     console.log('res: ', res);
//     
//     console.log('----');
//     console.log('Find one test:');
//     Meteor.call('linkedAccounts.findOne', '5b673f936f0efaddd718399a', (error, documentId) => {
//       if (error) {
//         Bert.alert(error.reason, 'danger');
//       } else {
//         Bert.alert('success', 'success');
//         console.log('res: ', documentId);
//       }
//     });
//     
//     console.log('----');

    
//     Meteor.subscribe('users.getUser', 'jonathanjoestar');
//     Meteor.call('users.getUserByUsername', 'jonathanjoestar', (error, documentId) => {
//       if (error) {
//         Bert.alert(error.reason, 'danger');
//       } else {
//         Bert.alert('success', 'success');
//         console.log('res: ', documentId);
//       }
//     });
//     console.dir(Meteor.users.find().fetch());
    
  }

  renderOAuthUser(loading, user) {
    return !loading ? (
      <div className="OAuthProfile">
        <div key={user.service} className={`LoggedInWith ${user.service}`}>
          <img src={`/${user.service}.svg`} alt={user.service} />
          <p>{`You're logged in with ${_.capitalize(user.service)} using the email address ${user.emails[0].address}.`}</p>
          <Button
            className={`btn btn-${user.service}`}
            href={{
              facebook: 'https://www.facebook.com/settings',
              google: 'https://myaccount.google.com/privacy#personalinfo',
              github: 'https://github.com/settings/profile',
            }[user.service]}
            target="_blank"
          >
            Edit Settings on {_.capitalize(user.service)}
          </Button>
        </div>
      </div>) : <div />;
  }

  renderPasswordUser(loading, user) {
    return !loading ? (
      <div>
        <Row>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>Username</ControlLabel>
              <input
                type="text"
                name="username"
                defaultValue={user.profile.username}
                className="form-control"
              />
            </FormGroup>
          </Col>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>Full Name</ControlLabel>
              <input
                type="text"
                name="fullname"
                defaultValue={user.profile.fullname}
                className="form-control"
              />
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <ControlLabel>Email Address</ControlLabel>
          <input
            type="email"
            name="emailAddress"
            defaultValue={user.emails[0].address}
            className="form-control"
          />
        </FormGroup>
        <Row>
          <Col xs={6}>
            <FormGroup>
              <ControlLabel>STEEM account</ControlLabel>
              <input
                type="text"
                name="steemAccount"
                defaultValue={user.accounts && user.accounts.steem}
                className="form-control"
                disabled
              />
            </FormGroup>
          </Col>
          <Col xs={6}>
            <Button bsStyle="link" className="btn-export" onClick={this.handleLinkSteem}>Link account</Button>
            <Button bsStyle="link" className="btn-export" onClick={this.handleSubscriptionTest}>Subscribe</Button>
          </Col>
        </Row>
        <FormGroup>
          <ControlLabel>GOLOS account</ControlLabel>
          <input
            type="text"
            name="golosAccount"
            defaultValue={user.accounts && user.accounts.golosAccount && user.accounts.golosAccount.username}
            className="form-control"
          />
        </FormGroup>
        
        <FormGroup>
          <ControlLabel>Current Password</ControlLabel>
          <input
            type="password"
            name="currentPassword"
            className="form-control"
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>New Password</ControlLabel>
          <input
            type="password"
            name="newPassword"
            className="form-control"
          />
          <InputHint>Use at least six characters.</InputHint>
        </FormGroup>
        <Button type="submit" bsStyle="success">Save Profile</Button>
      </div>
    ) : <div />;
  }

  renderProfileForm(loading, user) {
    return !loading ? ({
      password: this.renderPasswordUser,
      oauth: this.renderOAuthUser,
    }[this.getUserType(user)])(loading, user) : <div />;
  }

  render() {
    const { loading, user } = this.props;
    return (
      <StyledProfile>
        <Grid>
          <Row>
            <Col lg={6} lgOffset={3} md={8} mdOffset={2} sm={10} smOffset={1} xs={12} >
              <h4 className="page-header">{user && user.profile ? `${user.profile.fullname}` : user.profile.username}</h4>
              <Tabs animation={false} activeKey={this.state.activeTab} onSelect={activeTab => this.setState({ activeTab })} id="admin-user-tabs">
                <Tab eventKey="profile" title="Profile">
                  <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
                    {this.renderProfileForm(loading, user)}
                  </form>
                  <AccountPageFooter>
                    <p><Button bsStyle="link" className="btn-export" onClick={this.handleExportData}>Export my data</Button> – Download all of your documents as .txt files in a .zip</p>
                  </AccountPageFooter>
                  <AccountPageFooter>
                    <Button bsStyle="danger" onClick={this.handleDeleteAccount}>Delete My Account</Button>
                  </AccountPageFooter>
                </Tab>
                <Tab eventKey="settings" title="Settings">
                  { /* Manually check the activeTab value to ensure we refetch settings on re-render of UserSettings */ }
                  {this.state.activeTab === 'settings' ? <UserSettings /> : ''}
                </Tab>
              </Tabs>
            </Col>
          </Row>
        </Grid>
      </StyledProfile>
    );
  }
}

Settings.propTypes = {
  loading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
};

export default withTracker(() => {
  const subscription = Meteor.subscribe('users.editProfile');
//   const linkedAccountsSub = Meteor.subscribe('linkedAccounts');

  return {
    loading: !subscription.ready(),
//     loadingLinked: !linkedAccountsSub.ready(),
    user: getUserProfile(Meteor.users.findOne({ _id: Meteor.userId() })),
//     linkedAccount: Meteor.LinkedAccounts.findOne({ _id: Meteor.userId() }),
  };
})(Settings);
