import React from 'react';
import autoBind from 'react-autobind';
import { Grid, Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Bert } from 'meteor/themeteorchef:bert';
import OAuthLoginButtons from '../../components/OAuthLoginButtons/OAuthLoginButtons';
import InputHint from '../../components/InputHint/InputHint';
import AccountPageFooter from '../../components/AccountPageFooter/AccountPageFooter';
import validate from '../../../modules/validate';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  componentDidMount() {
    const component = this;


    validate(component.form, {
      rules: {
        username: {
          required: true,
        },
        fullname: {
          required: false,
        },
        emailAddress: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 6,
        },
      },
      messages: {
        username: {
          required: 'What\'s your username?',
        },
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?',
        },
        password: {
          required: 'Need a password here.',
          minlength: 'Please use at least six characters.',
        },
      },
      submitHandler() { component.handleSubmit(component.form); },
    });
  }

  handleSubmit(form) {
    console.dir(form);
    const { history } = this.props;

    Accounts.createUser({
      email: form.emailAddress.value,
      password: form.password.value,
      profile: {
        username: form.username.value,
        fullname: form.fullname.value,
      },
      accounts: {},
    }, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Meteor.call('users.sendVerificationEmail');
        Bert.alert('Welcome!', 'success');
        history.push('/documents');
      }
    });
  }

//   render() {
//     return (
//       <div className="Signup">
//         <Grid>
//         <Row>
//           <Col lg={6} lgOffset={3} md={8} mdOffset={2} sm={10} smOffset={1} xs={12} >
//             <h4 className="page-header">Sign Up</h4>
//             <Row>
//               <Col xs={12}>
//                 <OAuthLoginButtons
//                   services={['facebook', 'github', 'google']}
//                   emailMessage={{
//                     offset: 97,
//                     text: 'Sign Up with an Email Address',
//                   }}
//                 />
//               </Col>
//             </Row>
//             <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
//               <Row>
//                 <Col xs={6}>
//                   <FormGroup>
//                     <ControlLabel>First Name</ControlLabel>
//                     <input
//                       type="text"
//                       name="firstName"
//                       className="form-control"
//                     />
//                   </FormGroup>
//                 </Col>
//                 <Col xs={6}>
//                   <FormGroup>
//                     <ControlLabel>Last Name</ControlLabel>
//                     <input
//                       type="text"
//                       name="lastName"
//                       className="form-control"
//                     />
//                   </FormGroup>
//                 </Col>
//               </Row>
//               <FormGroup>
//                 <ControlLabel>Email Address</ControlLabel>
//                 <input
//                   type="email"
//                   name="emailAddress"
//                   className="form-control"
//                 />
//               </FormGroup>
//               <FormGroup>
//                 <ControlLabel>Password</ControlLabel>
//                 <input
//                   type="password"
//                   name="password"
//                   className="form-control"
//                 />
//                 <InputHint>Use at least six characters.</InputHint>
//               </FormGroup>
//               <Button type="submit" bsStyle="success">Sign Up</Button>
//               <AccountPageFooter>
//                 <p>Already have an account? <Link to="/login">Log In</Link>.</p>
//               </AccountPageFooter>
//             </form>
//           </Col>
//         </Row>
//         </Grid>
//       </div>
//     );
//   }
  
  render() {
    const { productName } = Meteor.settings.public;
    return (
      <div className="Signup">
        <Grid>
          <Row>
            <Col lg={6} lgOffset={3} md={8} mdOffset={2} sm={10} smOffset={1} xs={12} >
              <div className="login_area">
                <div className="login_row">
                  <img src="/placeholders/login-logo.png" />
                  <h4>Create a new account on { productName } or <Link to="/login">Log in</Link></h4> 
                  <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()} className="input_group"> 
                    <div className="input-field">
                      <input type="text" className="validate" name="username" placeholder="Username" /> 
                      <input type="text" className="validate" name="fullname" placeholder="Full Name (optional)" /> 
                      <input type="email" className="validate" name="emailAddress" placeholder="Email" />
                      <input type="password" className="validate" name="password" placeholder="Password" />
                    </div>
                    <p>
                      <input type="checkbox" id="terms" />
                      <label htmlFor="terms">I accept the <Link to="/terms">Terms and Services</Link></label>
                    </p>
                    <button type="submit" className="waves-effect">Sign Up</button>
                  </form> 
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Signup.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Signup;
