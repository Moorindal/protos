/* eslint-disable no-underscore-dangle */

import React from 'react'
import PropTypes from 'prop-types'
import autoBind from 'react-autobind'
import FileSaver from 'file-saver'
import base64ToBlob from 'b64-to-blob'
import { Row, Col, FormGroup, ControlLabel, Button, Tabs, Tab } from 'react-bootstrap'
import _ from 'lodash'
import styled from 'styled-components'
import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'
import { Bert } from 'meteor/themeteorchef:bert'
import { Link } from 'react-router-dom'
import { withTracker } from 'meteor/react-meteor-data'
import { compose } from 'redux'
import { connect } from 'react-redux'
import InputHint from '../../components/InputHint/InputHint'
import AccountPageFooter from '../../components/AccountPageFooter/AccountPageFooter'
import PostPreview from '../../components/PostPreview/PostPreview'
import Feed from '../../components/Feed/Feed'
import SidebarPosting from '../../components/SidebarPosting/SidebarPosting'
import UserSettings from '../../components/UserSettings/UserSettings'
import validate from '../../../modules/validate'
import getUserProfile from '../../../modules/get-user-profile'
import buildUserFeed from '../../../modules/feed-controller'
import handleMethodException from '../../../modules/handle-method-exception'



class Profile extends React.Component {
  constructor(props) {
    console.log('Profile constructor...', props)
    super(props)
    autoBind(this)

//     console.log('feed state ', this.props)
    
  }

//   componentWillMount() {
//     console.log('Profile componentWillMount...')
//   }
//   
//   componentDidMount() {
//     console.log('Profile componentDidMount...')
//     console.dir(this.props.profileUser)
//     const component = this
//   }
//   
//   componentWillUnmount() {
//     console.log('Profile componentWillUnmount...')
//     this.setState({ feedData: {}, feedLoaded: false })
//   }
  

  profileReady(user) {
    console.log('profileReady for ', user)
    console.log('feed state ', this.props.feed)
    
    if (this.props.feed.for != user.profile.username) {
      console.log('feed requested');
//       this.once = true;
      this.props.feed.loading = true;
      buildUserFeed({username: user.profile.username, accounts: user.accounts})
      .then((response) => {
        this.props.setFeedAction({
          data: response,
          loading: false,
          for: user.profile.username,
        });
        console.log('Setting feed ', this.props.feed);
      })
      .catch((exception) => {
        handleMethodException(exception);
      });
    }
    
//     this.renderProfile(user);
  }
    
  renderProfile() {
    if (this.props.loading) return false;
    let user = this.props.profileUser
    user.accounts && this.profileReady(user)
    console.log('renderProfile')
    console.log('feed state ', this.props.feed)
    
    return (
      <>
        <div className="banner_area banner_2">
            <img src="/placeholders/banner-2.jpg" alt="" className="banner_img" />  
            <div className="media profile_picture">
                <Link to="/profile"><img src="/placeholders/profile-hed-1.jpg" alt="" className="circle" /></Link> 
                <div className="media_body">
                    <Link to="/profile">{user.profile.fullname || user.profile.username}</Link>
                    <h6>Dhaka, Bangladesh</h6>
                </div>
            </div>
        </div>
      <section className="author_profile">
          <div className="row author_profile_row">
              <div className="col l4 m6"> 
                  <ul className="profile_menu">
                      <li><a href="#">About</a></li>
                      <li><a href="#">Photos</a></li>
                      <li><a href="#">Videos</a></li>
                      <li className="post_d"><a className="dropdown-button" href="#!" data-activates="dro_pm">...</a>
                          <ul id="dro_pm" className="dropdown-content">
                              <li><a href="#">Popular Post</a></li> 
                              <li><a href="#">Save Post</a></li> 
                          </ul>
                      </li>
                  </ul>
              </div>
              <div className="col l4 m6"> 
                  <ul className="post_follow">
                      <li>Posts <b>102</b></li>
                      <li>Followers <b>389</b></li>
                      <li>Following <b>51</b></li>
                  </ul>
              </div>
              <div className="col l4 m6"> 
                  <ul className="follow_messages">
                      <li><a href="#" className="waves-effect">Follow</a></li>
                      <li><a href="#" className="waves-effect">Messages</a></li>
                  </ul>
              </div>
          </div>
      </section>

      <section className="min_container profile_pages">
          <div className="section_row">
              <div id="profile-feed" className="middle_section col">
              
                { this.props.feed && !this.props.feed.loading && this.props.feed.for == user.profile.username ? <Feed data={this.props.feed.data} /> : false }
                  
                  <div className="pagination_area"> 
                      <ul className="pagination">
                          <li className="disabled"><a href="#!"><i className="ion-chevron-left"></i></a></li>
                          <li className="active"><a href="#!" className="waves-effect">1</a></li>
                          <li><a href="#!" className="waves-effect">2</a></li>
                          <li><a href="#!" className="waves-effect">3</a></li>
                          <li><a href="#!" className="waves-effect">4</a></li>
                          <li><a href="#!" className="waves-effect">...</a></li>
                          <li><a href="#!" className="waves-effect">20</a></li>
                          <li><a href="#!" className="waves-effect"><i className="ion-chevron-right"></i></a></li>
                      </ul>
                  </div>
              </div>

              <div className="col">
                  <div className="left_side_bar">
                      <div className="categories">
                          <h3 className="categories_tittle me_tittle">About Me</h3>
                          <p>I focus a lot on helping the first time or inexperienced traveler head out prepared and confident in themselves. Starting out as a old traveler can be intimidating. How do you jump into the gigantic travel fray and survive? How can you learn to love delays and long lines?</p>
                      </div>
                      <div className="interests">
                          <h3 className="categories_tittle">Your Interests <span>Edit</span></h3> 
                          <ul className="interests_list">
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Arts</a></li>
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Beauty</a></li>
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Entertainment</a></li>
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Travel</a></li>
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Personal</a></li>
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Politics</a></li>
                              <li><a href="#"><i className="ion-android-radio-button-off"></i>Space Science</a></li>
                          </ul>
                      </div>
                      <div className="profile">
                          <h3 className="categories_tittle">Profile <span>Edit</span></h3> 
                          <ul className="profile_pic">
                              <li><a href=""><img src="/placeholders/profile-1.jpg" alt="" className="circle" /></a></li>
                              <li><a href=""><img src="/placeholders/profile-2.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-3.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-4.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-5.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-6.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-7.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-8.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-9.jpg" alt="" className="circle" /></a></li> 
                              <li><a href=""><img src="/placeholders/profile-10.jpg" alt="" className="circle" /></a></li> 
                          </ul>
                      </div>

                      <div className="advertis">
                          <a href="#"><img src="/placeholders/advertis.jpg" alt="" /></a>
                      </div>
                  </div>
              </div>
              <div className="right_side_bar col">
                  <div className="right_sidebar_iner"> 
                      <div className="popular_posts popular_fast">
                          <h3 className="categories_tittle">My Submission</h3>
                          <SidebarPosting type="activity"
                                          action="submitted a new photo"
                                          item="How To Talk With Girls"
                                          date="2 days ago" />
                                             
                          <SidebarPosting type="activity"
                                          action="contributed a new paragraph"
                                          item="10 Ways To Make Easy Suicide"
                                          date="3 days ago" />
                                             
                          <SidebarPosting type="activity"
                                          action="submitted 10 photos"
                                          item="Best Photos of The Tech Giants"
                                          date="4 days ago" />
                                             
                          <SidebarPosting type="activity"
                                          action="submitted a new photo"
                                          item="How To Talk With Girls"
                                          date="5 days ago" />
                                             
                          <SidebarPosting type="activity"
                                          action="submitted a new photo"
                                          item="10 Ways To Make Easy Suicide"
                                          date="7 days ago" />
                                             
                      </div>
                      <div className="popular_gallery row"> 
                          <h3 className="categories_tittle">Images</h3> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-1.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-2.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-3.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-4.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-5.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-6.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-7.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-8.jpg" alt="" /></a></div> 
                          <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-9.jpg" alt="" /></a></div> 
                      </div>
                      <div className="trending_area">
                          <h3 className="categories_tittle">Trending</h3> 
                          <ul className="collapsible trending_collaps" data-collapsible="accordion">
                              <li>
                                  <div className="collapsible-header"><i className="ion-chevron-right"></i>Healthy Environment For Self Esteem</div>
                                  <div className="collapsible-body">
                                      <div className="row collaps_wrpper"> 
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div>
                                      <div className="row collaps_wrpper collaps_2">
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div> 
                                  </div>
                              </li>
                              <li>
                                  <div className="collapsible-header"><i className="ion-chevron-right"></i>Burn The Ships</div>
                                  <div className="collapsible-body">
                                      <div className="row collaps_wrpper"> 
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div>
                                      <div className="row collaps_wrpper collaps_2">
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div> 
                                  </div>
                              </li>
                              <li>
                                  <div className="collapsible-header active"><i className="ion-chevron-right"></i>Harness The Power Of Your Dreams</div>
                                  <div className="collapsible-body">
                                      <div className="row collaps_wrpper"> 
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div>
                                      <div className="row collaps_wrpper collaps_2">
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div> 
                                  </div>
                              </li>
                              <li>
                                  <div className="collapsible-header"><i className="ion-chevron-right"></i>Don T Let The Outtakes Take You Out</div>
                                  <div className="collapsible-body">
                                      <div className="row collaps_wrpper"> 
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div>
                                      <div className="row collaps_wrpper collaps_2">
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div> 
                                  </div>
                              </li>
                              <li>
                                  <div className="collapsible-header"><i className="ion-chevron-right"></i>Helen Keller A Teller And A Seller</div>
                                  <div className="collapsible-body"> 
                                      <div className="row collaps_wrpper collaps_2">
                                          <div className="col s1 media_l">
                                              <b>1</b>
                                              <i className="ion-android-arrow-dropdown-circle"></i>
                                          </div>
                                          <div className="col s11 media_b">
                                              <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                              <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                              <h6>By <a href="#">Thomas Omalley</a></h6>
                                          </div>
                                      </div> 
                                  </div>
                              </li>
                          </ul> 
                      </div>
                  </div>
              </div>
          </div>
        </section>
      </>
    )
  }
  
  
  render() {
    return (
      <article>
        {this.props.profileUser ? this.renderProfile() : false}
      </article>
    )
  }
}

Profile.defaultProps = {
  feed: {
    data: {},
    loading: false,
    for: false,
  }
};


export default compose(
  connect(state => ({ ...state })),
  withTracker(({ match }) => {
    const userToLoad = match.params.username
//     if (Meteor.isClient) {
      console.log('subscribing to users')
      const sub = Meteor.subscribe('users.getUser', userToLoad)
      const loading = !sub.ready()
      const profileUser = Meteor.users.findOne({'profile.username': userToLoad})
      
//     }

    return {
      profileUser,
      loading,
    }
  }),
)(Profile);
