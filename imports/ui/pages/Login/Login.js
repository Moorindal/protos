import React from 'react';
import autoBind from 'react-autobind';
import { Grid, Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import OAuthLoginButtons from '../../components/OAuthLoginButtons/OAuthLoginButtons';
import AccountPageFooter from '../../components/AccountPageFooter/AccountPageFooter';
import validate from '../../../modules/validate';

class Login extends React.Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  componentDidMount() {
    const component = this;

    validate(component.form, {
      rules: {
        emailAddress: {
          required: true,
          email: true,
        },
        password: {
          required: true,
        },
      },
      messages: {
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?',
        },
        password: {
          required: 'Need a password here.',
        },
      },
      submitHandler() { component.handleSubmit(component.form); },
    });
  }

  handleSubmit(form) {
    Meteor.loginWithPassword(form.emailAddress.value, form.password.value, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Welcome back!', 'success');
      }
    });
  }

//   render() {
//     return (
//       <div className="Login">
//         <Row>
//           <Col xs={12} sm={6} md={5} lg={4}>
//             <h4 className="page-header">Log In</h4>
//             <Row>
//               <Col xs={12}>
//                 <OAuthLoginButtons
//                   services={['facebook', 'github', 'google']}
//                   emailMessage={{
//                     offset: 100,
//                     text: 'Log In with an Email Address',
//                   }}
//                 />
//               </Col>
//             </Row>
//             <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
//               <FormGroup>
//                 <ControlLabel>Email Address</ControlLabel>
//                 <input
//                   type="email"
//                   name="emailAddress"
//                   className="form-control"
//                 />
//               </FormGroup>
//               <FormGroup>
//                 <ControlLabel className="clearfix">
//                   <span className="pull-left">Password</span>
//                   <Link className="pull-right" to="/recover-password">Forgot password?</Link>
//                 </ControlLabel>
//                 <input
//                   type="password"
//                   name="password"
//                   className="form-control"
//                 />
//               </FormGroup>
//               <Button type="submit" bsStyle="success">Log In</Button>
//               <AccountPageFooter>
//                 <p>{'Don\'t have an account?'} <Link to="/signup">Sign Up</Link>.</p>
//               </AccountPageFooter>
//             </form>
//           </Col>
//         </Row>
//       </div>
//     );
//   }
  
  render() {
    const { productName } = Meteor.settings.public;
    
    return (
      <div className="Login">
        <Grid>
          <Row>
            <Col lg={6} lgOffset={3} md={8} mdOffset={2} sm={10} smOffset={1} xs={12} >
              <div className="login_area">
                <div className="login_row">
                  <img src="/placeholders/login-logo.png" />
                  <h4>Log in to { productName } or <Link to="/signup">create an account</Link></h4>
                  <h6>Sign in using social media</h6>
                  <ul className="with_social">
                    <li><a href="#" className="waves-effect"><i className="icon ion-logo-twitter"></i></a></li>
                    <li><a href="#" className="waves-effect facebook"><i className="icon ion-logo-facebook"></i></a></li>
                    <li><a href="#" className="waves-effect google-plus"><i className="icon ion-logo-googleplus"></i></a></li>
                  </ul>
                  <h6>or log in using email address</h6>
                  <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()} className="input_group"> 
                    <div className="input-field">
                      <input type="email" name="emailAddress" className="validate" placeholder="Email" /> 
                      <input type="password" name="password" className="validate" placeholder="Password" /> 
                    </div>
                    <p>
                      <input type="checkbox" id="rememberMe" />
                      <label htmlFor="rememberMe">Remember me <Link className="pull-right" to="/recover-password">Forgot password?</Link></label>
                    </p>
                    <button className="waves-effect">Log In</button>
                  </form>
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Login;
