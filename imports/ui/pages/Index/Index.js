import React from 'react';
import { Button } from 'react-bootstrap';
import styled from 'styled-components';
import { lighten, darken } from 'polished';
import PostPreview from '../../components/PostPreview/PostPreview';
import SidebarPosting from '../../components/SidebarPosting/SidebarPosting';


const Index = () => (
  <article>
    <ul className="tranding_select">
      <li><a href="#" className="waves-effect btn">Latest</a></li>
      <li><a href="#" className="waves-effect btn">Trending</a></li>
      <li><a href="#" className="waves-effect btn">Featured</a></li>  
    </ul>
    <div className="banner_area">
        <img src="/placeholders/banner.jpg" className="banner_img" />
    </div>
    <section className="min_container min_pages">
        <div className="section_row">
            <div className="middle_section col" id="infinite_scroll">
               
              <div className="fast_post">

                <PostPreview author="Masum Rana" id="4"
                               category="Technology"
                               title="Choosing The Best Audio Player Software For Your Computer"
                               lead="In the last 10 years Americans have seen a boom in local food markets and for good reason. While Americans continue to buy more fast food, they still expect perfect ingredients and they are finding them."
                               />
              </div>

                <PostPreview author="Jason Borne" id="5"
                               category="Advertising"
                               title="The Best Way To Fight Poor Health Is To Make Home Cooking Fast And Easy"
                               lead="In the last 10 years Americans have seen a boom in local food markets and for good reason. While Americans continue to buy more fast food, they still expect perfect ingredients and they are finding them."
                               />
                               
                <PostPreview author="Masum Rana" id="6"
                               category="Technology"
                               title="Get Around Easily With A New York Limousine Service"
                               lead="In the last 10 years Americans have seen a boom in local food markets and for good reason. While Americans continue to buy more fast food, they still expect perfect ingredients and they are finding them."
                               />
                

                <PostPreview author="Jason Borne" id="7"
                               category="Advertising"
                               title="Skin Cancer Prevention 5 Ways To Protect Yourself From Uv Rays"
                               lead="In the last 10 years Americans have seen a boom in local food markets and for good reason. While Americans continue to buy more fast food, they still expect perfect ingredients and they are finding them."
                               />
               
 
                <a href="load-mor.html" className="load-mor btn-large">Loding next</a>
            </div>

            <div className="col">
                <div className="left_side_bar">
                    <div className="categories">
                        <h3 className="categories_tittle">Post Categories</h3>
                        <ul className="categories_icon"> 
                            <li><a href="#" data-balloon="Rate Post" data-balloon-pos="up"><i className="icon ion-md-star"></i></a></li> 
                            <li><a href="#" data-balloon-pos="up"><i className="icon ion-md-time"></i></a></li>
                            <li><a href="#" data-balloon="Music Post" data-balloon-pos="up"><i className="icon ion-md-airplane"></i></a></li>
                            <li><a href="#" data-balloon="Images Post" data-balloon-pos="up"><i className="icon ion-md-images"></i></a></li>
                            <li><a href="#" data-balloon="chart Post" data-balloon-pos="up"><i className="icon ion-md-stats"></i></a></li>  
                        </ul>
                    </div>
                    <div className="interests">
                        <h3 className="categories_tittle">Your Interests <span>Edit</span></h3> 
                        <ul className="interests_list">
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Arts</a></li>
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Beauty</a></li>
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Entertainment</a></li>
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Travel</a></li>
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Personal</a></li>
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Politics</a></li>
                            <li><a href="#"><i className="ion-android-radio-button-off"></i>Space Science</a></li>
                        </ul>
                    </div>
                    <div className="profile">
                        <h3 className="categories_tittle">Profile <span>Edit</span></h3> 
                        <ul className="profile_pic">
                            <li><a href=""><img src="/placeholders/profile-1.jpg" className="circle" /></a></li>
                            <li><a href=""><img src="/placeholders/profile-2.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-3.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-4.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-5.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-6.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-7.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-8.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-9.jpg" className="circle" /></a></li> 
                            <li><a href=""><img src="/placeholders/profile-10.jpg" className="circle" /></a></li> 
                        </ul>
                    </div>

                    <div className="advertis">
                        <a href="#"><img src="/placeholders/advertis.jpg" /></a>
                    </div>
                </div>
            </div>
            
            <div className="right_side_bar col">
                <div className="right_sidebar_iner">
                    <a href="#"> 
                        <img src="/placeholders/advertis-2.jpg" className="responsive-img" />
                    </a>
                    <div className="popular_posts">
                        <h3 className="categories_tittle">Popular Posts</h3>  
                        <SidebarPosting item="Poster can be one of the effective marketing and advertising materials."
                                        date="1 days ago" />
                                        
                        <SidebarPosting item="Color is so powerful that it can persuade, motivate, inspire and touch."
                                        date="2 days ago" />
                        
                        <SidebarPosting item="What makes one logo better than another?"
                                        date="3 days ago" />
                                        
                        <SidebarPosting item="Outdoor advertising is a low budget and effective way of advertising a"
                                        date="4 days ago" />
                        
                        <SidebarPosting item="Famous is as famous does and the famous get known through publicity."
                                        date="5 days ago" />
                                        
                    </div>
                    <div className="popular_gallery row"> 
                        <h3 className="categories_tittle">Images</h3> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-1.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-2.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-3.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-4.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-5.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-6.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-7.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-8.jpg" /></a></div> 
                        <div className="col s4 p_img"><a href="#"><img src="/placeholders/gallery/gallry-s-9.jpg" /></a></div> 
                    </div>
                    <div className="trending_area">
                        <h3 className="categories_tittle">Trending</h3> 
                        <ul className="collapsible trending_collaps" data-collapsible="accordion">
                            <li>
                                <div className="collapsible-header"><i className="ion-chevron-right"></i>Healthy Environment For Self Esteem</div>
                                <div className="collapsible-body">
                                    <div className="row collaps_wrpper"> 
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div>
                                    <div className="row collaps_wrpper collaps_2">
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div> 
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="ion-chevron-right"></i>Burn The Ships</div>
                                <div className="collapsible-body">
                                    <div className="row collaps_wrpper"> 
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div>
                                    <div className="row collaps_wrpper collaps_2">
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div> 
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header active"><i className="ion-chevron-right"></i>Harness The Power Of Your Dreams</div>
                                <div className="collapsible-body">
                                    <div className="row collaps_wrpper"> 
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div>
                                    <div className="row collaps_wrpper collaps_2">
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div> 
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="ion-chevron-right"></i>Don T Let The Outtakes Take You Out</div>
                                <div className="collapsible-body">
                                    <div className="row collaps_wrpper"> 
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem knowing what to pack, but instead have</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div>
                                    <div className="row collaps_wrpper collaps_2">
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div> 
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="ion-chevron-right"></i>Helen Keller A Teller And A Seller</div>
                                <div className="collapsible-body"> 
                                    <div className="row collaps_wrpper collaps_2">
                                        <div className="col s1 media_l">
                                            <b>1</b>
                                            <i className="ion-android-arrow-dropdown-circle"></i>
                                        </div>
                                        <div className="col s11 media_b">
                                            <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                            <p>The Emerald Buddha is a figurine of a sitting Budha, that is the is the palladium</p>
                                            <h6>By <a href="#">Thomas Omalley</a></h6>
                                        </div>
                                    </div> 
                                </div>
                            </li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </section>
  </article>
);

export default Index;
