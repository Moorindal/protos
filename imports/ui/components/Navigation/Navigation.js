import React from 'react';
import PropTypes from 'prop-types';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import PublicNavigation from '../PublicNavigation/PublicNavigation';
import AuthenticatedNavigation from '../AuthenticatedNavigation/AuthenticatedNavigation';

// const Navigation = props => (
//   <Navbar collapseOnSelect>
//     <Navbar.Header>
//       <Navbar.Brand>
//         <Link to="/">{Meteor.settings.public.productName}</Link>
//       </Navbar.Brand>
//       <Navbar.Toggle />
//     </Navbar.Header>
//     <Navbar.Collapse>
//       {!props.authenticated ? <PublicNavigation /> : <AuthenticatedNavigation {...props} />}
//     </Navbar.Collapse>
//   </Navbar>
// );

const Navigation = props => (
    <nav className="header_area"> 
        <div className="custom_container"> 
            <div className="nav-wrapper">
              <Link className="brand-logo" to="/"><img src="/placeholders/logo.png" alt="" /></Link>
                <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="icon ion-md-menu"></i></a>
                <a href="#post_modal" className="waves-effect btn post_btn sm_post_btn modal-trigger">
                  <i className="icon ion-md-add"></i>Add Post
                </a>
                <ul className="left_menu hide-on-med-and-down">
                    <li><Link to="/">Home</Link></li>  
                    <li className="user_dropdown"><a className="dropdown-button" href="#!" data-activates="dropdown6">All Pages</a>
                        <div id="dropdown6" className="dropdown-content submenu row"> 
                           <div className="col m4 menu_column">
                               <ul>
                                   <li><a href="get-started.html">Get started</a></li>
                                   {props.authenticated ? <li><Link to="/dashboard">Dashboard</Link></li> : false }
                                   <li><a href="details.html">Post details</a></li>
                                   <li><a href="details-2.html">Post details 2</a></li>
                                   {props.authenticated && props.username ? <li><Link to={"/profile/"+props.username} className="active">Profile</Link></li> : false }
                                   <li><Link to={"/profile/jonathanjoestar"} className="active">Profile 2</Link></li>
                               </ul> 
                           </div> 
                           <div className="col m4 menu_column">
                               <ul> 
                                   <li><a href="messages.html">Messages</a></li>
                                   <li><a href="requests.html">Requests</a></li>
                                   <li><a href="read-later.html">Read-Later</a></li>
                                   <li><a href="notifications.html">Notifications</a></li>
                                   <li><a href="block-list.html">Block-list</a></li>
                               </ul> 
                           </div>
                           <div className="col m4 menu_column">
                               <ul> 
                                   <li><a href="photos.html">Photos</a></li>
                                   <li><a href="photos-2.html">Photos v2</a></li>
                                   <li><a href="video.html">Videos</a></li>
                                   <li><a href="error.html">Error (404)</a></li>
                               </ul> 
                           </div>  
                            
                        </div> 
                    </li>
                    {props.authenticated && props.username ? <li><Link to={"/profile/"+props.username} className="active">Profile</Link></li> : false }
                    <li><Link to={"/profile/jonathanjoestar"} className="active">Profile 2</Link></li>
                    {props.authenticated ? <li><Link to="/dashboard">Dashboard</Link></li> : false }
                    <li className="notifications search_sm"><a className="dropdown-button" href="#!" data-activates="dropdown5"><i className="icon ion-md-search"></i></a>

                        <ul id="dropdown5" className="dropdown-content">  
                           <li className="search_from"> 
                                <input placeholder="Search and enter" type="text" /> 
                           </li>
                        </ul>
                    </li>
                </ul>
                <ul className="right right_menu hide-on-med-and-down">
                    <li className="search_min">
                       <div className="search_from"> 
                            <input placeholder="Search Here" type="text" />
                            <a href="#" className="search_icon"><i className="icon ion-md-search"></i></a> 
                       </div>
                    </li>
                    
                    <li>
                      {!props.authenticated ? <PublicNavigation /> : <AuthenticatedNavigation {...props} />}
                    </li>
                    
                </ul>
                <ul className="side-nav" id="mobile-demo">
                    <li className="search_min">
                       <div className="search_from"> 
                            <input placeholder="Search Here" type="text" />
                            <a href="#" className="search_icon"><i className="icon ion-md-search"></i></a> 
                       </div>
                    </li>
                    <li><a href="index.html">Home</a></li>
                    <li className="user_dropdown"><a className="dropdown-button" href="#!" data-activates="dropdown_s0">All Pages</a>
                    
                        <ul id="dropdown_s0" className="dropdown-content">  
                           <li><a href="get-started.html">Get started</a></li>
                           <li><a href="dashboard.html">Dashboard</a></li>
                           <li><a href="details.html">Post details</a></li>
                           <li><a href="details-2.html">Post details 2</a></li>
                           { props.username ? <li><Link to={"/profile/"+props.username}>Profile</Link></li> : false }
                           <li><a href="messages.html">Messages</a></li>
                           <li><a href="requests.html">Requests</a></li>
                           <li><a href="read-later.html">Read-Later</a></li>
                           <li><a href="notifications.html">Notifications</a></li>
                           <li><a href="block-list.html">Block-list</a></li>
                           <li><a href="photos.html">Photos</a></li>
                           <li><a href="photos-2.html">Photos v2</a></li>
                           <li><a href="video.html">Videos</a></li>
                           <li><a href="block-list.html">Error (404)</a></li>
                        </ul>
                    </li> 
                    <li><a href="#">Shortcodes</a></li>
                    <li><a href="#">All Elements</a></li>  
                    
                    <li className="notifications follow"><a className="dropdown-button" href="#!" data-activates="dropdown_s1"><i className="icon ion-md-person"></i><b className="n-number">7</b></a>

                        <ul id="dropdown_s1" className="dropdown-content">
                            <li className="hed_notic">Follow feed <span><i className="icon ion-md-cog"></i></span></li> 
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-9.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Dan Fisher</b> started following you.</p>
                                            <h6>544 mutual</h6>
                                            <div className="btn_group"> 
                                                <span className="waves-effect follow_b">Follow back</span>
                                                <span className="waves-effect">Block</span>
                                            </div>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-1.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>James Richardman </b>started following you.</p>
                                            <h6>32 mutual</h6>
                                            <div className="btn_group"> 
                                                <span className="waves-effect confirm"><i className="icon ion-md-checkmark"></i></span>
                                                <span className="waves-effect">Block</span>
                                            </div>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-12.jpg" alt="" className="circle responsive-img w_img" />  
                                        <div className="media_body">
                                            <p>You are now following <b>Meryl Streep</b></p>
                                            <h6>Check out her most recent updates.</h6> 
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-2.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>James Richardman </b>started following you.</p>
                                            <h6>90 mutual</h6>
                                            <div className="btn_group"> 
                                                <span className="waves-effect close_b"><i className="icon ion-md-close"></i></span>
                                                <span className="waves-effect">Block</span>
                                            </div>
                                        </div> 
                                   </div>
                               </a>  
                            </li> 
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-10.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Dan Fisher</b> started following you.</p>
                                            <h6>544 mutual</h6>
                                            <div className="btn_group"> 
                                                <span className="waves-effect follow_b">Follow back</span>
                                                <span className="waves-effect"><i className="icon ion-md-checkmark"></i></span>
                                            </div>
                                        </div> 
                                   </div>
                               </a>  
                            </li> 
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-11.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Edgar Hoover</b> started following you.</p> 
                                            <div className="btn_group left"> 
                                                <span className="waves-effect follow_b">Follow back</span>
                                                <span className="waves-effect">Block</span>
                                            </div>
                                        </div> 
                                   </div>
                               </a>  
                            </li> 
                            <li><a href="requests.html" className="waves-effect chack_all_btn">Check All Follow Requests</a></li>
                        </ul>
                    </li>
                    
                    <li className="notifications messages"><a className="dropdown-button" href="#!" data-activates="dropdown_s2"><i className="ion-ios-chatboxes-outline"></i><b className="n-number">3</b></a>

                        <ul id="dropdown_s2" className="dropdown-content">
                            <li className="hed_notic">Messages <span>Mark all as read <i className="icon ion-md-cog"></i></span></li> 
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-1.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <h4>Emran Khan <small>12:40pm</small></h4>
                                            <p>Listen, I need to talk to you about this!</p>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-3.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <h4>Masum Rana <small>2 hours ago</small></h4>
                                            <p>One of the best ways to make a great vacation quickly horrible is to choose the...</p>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media seen"> 
                                        <img src="/placeholders/profile-8.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <h4>Jakaria Khan <small>3 hours ago</small></h4>
                                            <p>Hi</p>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-5.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <h4>Papia Sultana <small>2 days ago</small></h4>
                                            <p>Hey Masum, I’m looking for you as a new actor for upcoming “Equalizer 2” movie...</p>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media seen"> 
                                        <img src="/placeholders/profile-7.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <h4>Samuel L. <small>5 days ago</small></h4>
                                            <p>Hello</p>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li><a href="messages.html" className="waves-effect chack_all_btn">Check All Messages</a></li>
                        </ul>
                    </li>

                    <li className="notifications"><a className="dropdown-button" href="#!" data-activates="dropdown_s3"><i className="ion-ios-bell-outline"></i><b className="n-number">5</b></a>

                        <ul id="dropdown_s3" className="dropdown-content">
                            <li className="hed_notic">Notifications <span>Mark all as read <i className="icon ion-md-cog"></i></span></li> 
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-6.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Dan Fisher</b> submitted a new photo  to a <small>post</small> post you are following.</p>
                                            <h6>5 Minute ago</h6>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-7.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>James Richardman </b>downvoted your <small>answer</small> in a post.</p>
                                            <h6>5 Minute ago</h6>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media seen"> 
                                        <img src="/placeholders/profile-8.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Margot Robbie</b> commented on your <small>photo</small>.</p>
                                            <h6>5 Minute ago</h6>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media"> 
                                        <img src="/placeholders/profile-9.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Peter Parker</b> is now following you.</p>
                                            <h6>5 Minute ago</h6>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li>
                               <a href="#">
                                   <div className="media seen"> 
                                        <img src="/placeholders/profile-10.jpg" alt="" className="circle responsive-img" />  
                                        <div className="media_body">
                                            <p><b>Dan Fisher </b> submitted a new photo  to a <small>post</small> you are following.</p>
                                            <h6>5 Minute ago</h6>
                                        </div> 
                                   </div>
                               </a>  
                            </li>
                            <li><a href="notifications.html" className="waves-effect chack_all_btn">Check All Notifications</a></li>
                        </ul>
                    </li>

                    <li className="user_dropdown">
                      <a className="dropdown-button" href="#!" data-activates="dropdown_s4">
                        <img src="/placeholders/profile-pic.png" alt="" className="circle p_2" />
                      </a>
                        <ul id="dropdown_s4" className="dropdown-content" visibility="false">
                            { props.username ? <li><Link to={"/profile/"+props.username}><i className="icon ion-md-person"></i>My profile</Link></li> : false }
                            <li><a href="read-later.html"><i className="icon ion-md-folder-open"></i>Saved Articles</a></li> 
                            <li className="b_t"><a href="#"><i className="icon ion-md-notifications"></i>Notification settings</a></li>
                            <li className="b_b"><Link to="/settings"><i className="icon ion-md-lock"></i>Change Password</Link></li>
                            <li><Link to="/settings"><i className="icon ion-md-cog"></i>Settings</Link></li>
                            <li><a href="#"><i className="icon ion-md-flag"></i>Privacy Policy</a></li>
                            <li><a href="#"><i className="icon ion-md-podium"></i>FAQ</a></li>
                            <li><a href="/logout"><i className="icon ion-md-power"></i>Log out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
);

Navigation.defaultProps = {
  name: '',
};

Navigation.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  name: PropTypes.string,
};

export default Navigation;
