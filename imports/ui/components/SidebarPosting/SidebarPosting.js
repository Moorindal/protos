import React from 'react';
import PropTypes from 'prop-types';

const ActivityBlock = props => (
  <div>
    <a href="#">You {props.action} to <span>{props.item}</span></a>
    <span className="black_text">{props.date}</span>
  </div>
);

const PostBlock = props => (
  <div>
    <a href="#">{props.item}</a>
    <span className="black_text">{props.date}</span>
  </div>
);

const SidebarPosting = props => (
  <div className="row valign-wrapper popular_item">
    <div className="col s3 p_img">
      <a href="#"> 
        <img src="/placeholders/recent-post-2.jpg" alt="" className="circle responsive-img" />
      </a>
    </div>
    <div className="col s9 p_content">
      {props.type == 'activity' ? <ActivityBlock {...props} /> : <PostBlock {...props} /> }
    </div>
  </div>
);


export default SidebarPosting;
