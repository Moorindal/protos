import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import ImageSpin from '../../components/ImageSpin/ImageSpin';

// class PostPreview extends React.Component {
const PostPreview = props => (
  <section className="post">
    <div className="post_content"> 
      <a href="#" className="post_img">
          <ImageSpin image={props.image} alt={ props.title } />
          <span><i className="icon ion-md-radio-button-off"></i>{ props.category }</span>
      </a>
      <div className="row author_area">
        <div className="col s4 author">
          <div className="col s4 media_left"><img src="/placeholders/author-2.jpg" alt="" className="circle" /></div>
          <div className="col s8 media_body">
            <a href="#">{ props.author }</a>
            <span>5 Minute ago</span>
          </div>
        </div> 
        <div className="col s4 like_user offset-s4">
          <ul className="like_img">
            <li><a href="#" className="single_l_1"><img src="/placeholders/like-client-1.png" alt="" /></a></li>
            <li><a href="#" className="single_l"><img src="/placeholders/like-client-2.png" alt="" /></a></li>
            <li><a href="#" className="single_l"><img src="/placeholders/like-client-3.png" alt="" /></a></li>
            <li><a href="#" className="single_l"><img src="/placeholders/like-client-4.png" alt="" /></a></li>
            <li><a href="#" className="mor_like">+8 more</a></li> 
            <li className="post_d">
              <a className="dropdown-button waves-effect" href="#!" data-activates={'post_dropdown_'+props.id}>
                <i className="icon ion-md-more"></i>
              </a>
              <ul id={'post_dropdown_'+props.id} className="dropdown-content">
                <li><a href="#">Report as spam</a></li> 
                <li><a href="#">Read later</a></li> 
              </ul>
            </li>
          </ul>
        </div>
    </div>
    <a href="#" className="post_title">{ props.title }</a>
    <p>{ props.lead }</p>
    </div>
    <div className="like_comment_area row">
      <div className="col s4 btn_floating"> 
        <a className="btn-floating waves-effect"><i className="icon ion-md-share"></i></a>
        <h6>128k</h6>
      </div> 
      <div className="col s4 updown_btn">
        <a href="#"><i className="icon ion-md-arrow-dropdown-circle"></i></a>
        <a href="#"><i className="icon ion-md-arrow-dropup-circle"></i></a>
        <a href="#" className="count_n">483</a>
      </div>
      <div className="col s4 updown_btn comment_c"> 
        <a href="#"><i className="icon ion-md-chatbubbles"></i></a> 
        <a href="#" className="count_n">14</a>
      </div>  
    </div> 
  </section>
);


export default PostPreview;
