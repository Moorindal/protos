import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import PostPreview from '../../components/PostPreview/PostPreview';
import autoBind from 'react-autobind';

class Feed extends React.Component {
  constructor(props) {
    super(props);
    autoBind(this);
    this.state = { posts: this.processJson(this.props.data) };
//     console.log('Feed constructor, state: ', this.state);
  }
  
  processJson(json) {
//     console.log('Processing Json: ', json);
    
    var posts = [];
    
    json.forEach(post => {
      var metadata = JSON.parse(post.json_metadata)
      
      post.image = metadata.image ? metadata.image[0] : ''
      post.lead = post.body.substr(0, 360)
      post.tags = metadata.tags
      post.created = new Date(post.created).toDateString()

      posts.push(post);
    })
    
    return posts;

  }
  
  render() {
//     console.log('feedPage render ', this.state.posts);
    return (
      <div>
      {this.state && this.state.posts && this.state.posts.map(function(post){
        return (
          <PostPreview author={post.author} key={post.id}
                       category={post.tags[0]}
                       title={post.title}
                       lead={post.lead}
                       image={post.image}
          />
        )
       })}
      </div>
    );
  }
}

export default Feed;
