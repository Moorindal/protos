import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { Roles } from 'meteor/alanning:roles';

// const AuthenticatedNavigation = ({ name, history, userId }) => (
//   <div>
//     <Nav>
//       <LinkContainer to="/documents">
//         <NavItem eventKey={1} href="/documents">Documents</NavItem>
//       </LinkContainer>
//       {Roles.userIsInRole(userId, 'admin') ? <NavDropdown eventKey={2} title="Admin" id="admin-nav-dropdown">
//         <LinkContainer exact to="/admin/users">
//           <NavItem eventKey={2.1} href="/admin/users">Users</NavItem>
//         </LinkContainer>
//         <LinkContainer exact to="/admin/users/settings">
//           <NavItem eventKey={2.2} href="/admin/users/settings">User Settings</NavItem>
//         </LinkContainer>
//       </NavDropdown> : ''}
//     </Nav>
//     <Nav pullRight>
//       <NavDropdown eventKey={2} title={name} id="user-nav-dropdown">
//         <LinkContainer to="/profile">
//           <NavItem eventKey={2.1} href="/profile">Profile</NavItem>
//         </LinkContainer>
//         <MenuItem divider />
//         <MenuItem eventKey={2.2} onClick={() => history.push('/logout')}>Logout</MenuItem>
//       </NavDropdown>
//     </Nav>
//   </div>
// );

const AuthenticatedNavigation = props => (
  <section>
    <li><a href="#post_modal" className="waves-effect btn post_btn modal-trigger">
      <i className="icon ion-md-add"></i><span>Add Post</span>
    </a></li> 

    <li className="notifications follow"><a className="dropdown-button" href="#!" data-activates="dropdown4"><i className="icon ion-md-person"></i><b className="n-number">7</b></a>

        <ul id="dropdown4" className="dropdown-content">
            <li className="hed_notic">Follow feed <span><i className="icon ion-md-cog"></i></span></li> 
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-9.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Dan Fisher</b> started following you.</p>
                            <h6>544 mutual</h6>
                            <div className="btn_group"> 
                                <span className="waves-effect follow_b">Follow back</span>
                                <span className="waves-effect">Block</span>
                            </div>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-1.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>James Richardman </b>started following you.</p>
                            <h6>32 mutual</h6>
                            <div className="btn_group"> 
                                <span className="waves-effect confirm"><i className="icon ion-md-checkmark"></i></span>
                                <span className="waves-effect">Block</span>
                            </div>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-12.jpg" alt="" className="circle responsive-img w_img" />  
                        <div className="media_body">
                            <p>You are now following <b>Meryl Streep</b></p>
                            <h6>Check out her most recent updates.</h6> 
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-2.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>James Richardman </b>started following you.</p>
                            <h6>90 mutual</h6>
                            <div className="btn_group"> 
                                <span className="waves-effect close_b"><i className="icon ion-md-close"></i></span>
                                <span className="waves-effect">Block</span>
                            </div>
                        </div> 
                   </div>
               </a>
            </li> 
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-10.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Dan Fisher</b> started following you.</p>
                            <h6>544 mutual</h6>
                            <div className="btn_group"> 
                                <span className="waves-effect follow_b">Follow back</span>
                                <span className="waves-effect"><i className="icon ion-md-checkmark"></i></span>
                            </div>
                        </div> 
                   </div>
               </a>
            </li> 
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-11.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Edgar Hoover</b> started following you.</p> 
                            <div className="btn_group left"> 
                                <span className="waves-effect follow_b">Follow back</span>
                                <span className="waves-effect">Block</span>
                            </div>
                        </div> 
                   </div>
               </a>  
            </li> 
            <li><a href="requests.html" className="waves-effect chack_all_btn">Check All Follow Requests</a></li>
        </ul>
    </li>
    
    <li className="notifications messages"><a className="dropdown-button" href="#!" data-activates="dropdown3"><i className="icon ion-md-chatboxes"></i><b className="n-number">3</b></a>

        <ul id="dropdown3" className="dropdown-content">
            <li className="hed_notic">Messages <span>Mark all as read <i className="icon ion-md-cog"></i></span></li> 
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-1.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <h4>Emran Khan <small>12:40pm</small></h4>
                            <p>Listen, I need to talk to you about this!</p>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-3.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <h4>Masum Rana <small>2 hours ago</small></h4>
                            <p>One of the best ways to make a great vacation quickly horrible is to choose the...</p>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media seen"> 
                        <img src="/placeholders/profile-8.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <h4>Jakaria Khan <small>3 hours ago</small></h4>
                            <p>Hi</p>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-5.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <h4>Papia Sultana <small>2 days ago</small></h4>
                            <p>Hey Masum, I’m looking for you as a new actor for upcoming “Equalizer 2” movie...</p>
                        </div> 
                   </div>
               </a>
            </li>
            <li>
               <a href="#">
                   <div className="media seen"> 
                        <img src="/placeholders/profile-7.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <h4>Samuel L. <small>5 days ago</small></h4>
                            <p>Hello</p>
                        </div> 
                   </div>
               </a>
            </li>
            <li><a href="messages.html" className="waves-effect chack_all_btn">Check All Messages</a></li>
        </ul>
    </li>
    
    <li className="notifications"><a className="dropdown-button" href="#!" data-activates="dropdown2"><i className="icon ion-md-notifications"></i><b className="n-number">5</b></a>

        <ul id="dropdown2" className="dropdown-content">
            <li className="hed_notic">Notifications <span>Mark all as read <i className="icon ion-md-cog"></i></span></li> 
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-6.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Dan Fisher</b> submitted a new photo  to a <small>post</small> post you are following.</p>
                            <h6>5 Minute ago</h6>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-7.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>James Richardman </b>downvoted your <small>answer</small> in a post.</p>
                            <h6>5 Minute ago</h6>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media seen"> 
                        <img src="/placeholders/profile-8.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Margot Robbie</b> commented on your <small>photo</small>.</p>
                            <h6>5 Minute ago</h6>
                        </div> 
                   </div>
               </a>
            </li>
            <li>
               <a href="#">
                   <div className="media"> 
                        <img src="/placeholders/profile-9.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Peter Parker</b> is now following you.</p>
                            <h6>5 Minute ago</h6>
                        </div> 
                   </div>
               </a>  
            </li>
            <li>
               <a href="#">
                   <div className="media seen"> 
                        <img src="/placeholders/profile-10.jpg" alt="" className="circle responsive-img" />  
                        <div className="media_body">
                            <p><b>Dan Fisher </b> submitted a new photo  to a <small>post</small> you are following.</p>
                            <h6>5 Minute ago</h6>
                        </div> 
                   </div>
               </a>  
            </li>
            <li><a href="notifications.html" className="waves-effect chack_all_btn">Check All Notifications</a></li>
        </ul>
    </li>
    
    <li className="user_dropdown"><a className="dropdown-button" href="#!" data-activates="dropdown1">
      <img src="/placeholders/profile-pic.png" alt="" className="circle p_2" />
    </a>
    
        <ul id="dropdown1" className="dropdown-content">
            { props.username ? <li><Link to={"/profile/"+props.username}><i className="icon ion-md-person"></i>My profile</Link></li> : false }
            <li><a href="read-later.html"><i className="icon ion-md-folder-open"></i>Saved Articles</a></li> 
            <li className="b_t"><a href="#"><i className="icon ion-md-notifications"></i>Notification settings</a></li>
            <li className="b_b"><a href="#"><i className="icon ion-md-lock"></i>Change Password</a></li>
            <li><Link to="/settings"><i className="icon ion-md-cog"></i>Settings</Link></li>
            <li><a href="#"><i className="icon ion-md-flag"></i>Privacy Policy</a></li>
            <li><a href="#"><i className="icon ion-md-podium"></i>FAQ</a></li>
            <li><a href="/logout"><i className="icon ion-md-power"></i>Log out</a></li>
        </ul>
    </li>
  </section>
);

AuthenticatedNavigation.propTypes = {
  username: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(AuthenticatedNavigation);
