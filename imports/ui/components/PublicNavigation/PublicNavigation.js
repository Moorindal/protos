import React from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';

// const PublicNavigation = () => (
//   <Nav pullRight>
//     <LinkContainer to="/signup">
//       <NavItem eventKey={1} href="/signup">Sign Up</NavItem>
//     </LinkContainer>
//     <LinkContainer to="/login">
//       <NavItem eventKey={2} href="/login">Log In</NavItem>
//     </LinkContainer>
//   </Nav>
// );

const PublicNavigation = () => (
  <section>
    <li>
      <Link to="/signup">Sign Up</Link>
    </li>
    <li>
      <Link to="/login">Log In</Link>
    </li>
  </section>
);

export default PublicNavigation;
