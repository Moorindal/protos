import React from 'react';
import PropTypes from 'prop-types';

// const Spinner = () => (<img src="/Gear-2.5s-100px.svg" />);
// const Fallback = () => (<img src="/placeholders/post-2.jpg" />);
// 
// const components = {
//   loading: Spinner,
//   failed: Fallback
// }
const defaultImage = "/placeholders/post-2.jpg"

class ImageSpin extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.image == defaultImage ? {imageStatus: "failed"} : {imageStatus: "loading" };
//     console.dir(props);
  }

  handleImageLoaded() {
//     console.log(this.props.image+" is loaded!");
    this.setState({ imageStatus: "loaded" });
  }

  handleImageErrored() {
    console.log(this.props.image+" is failed to load!");
    this.setState({ imageStatus: "failed" });
  }
  
  renderSpinner() {
    return (<img src="/Gear-2.5s-100px.svg" />);
  }
  
  renderFallback() {
    return (<img src={defaultImage} />);
  }

  render() {
    return (
      <div>
        <img
          src={this.props.image}
          onLoad={this.handleImageLoaded.bind(this)}
          onError={this.handleImageErrored.bind(this)}
          alt={this.props.alt}
          hidden={this.state.imageStatus != 'loaded' && 'hidden'}
        />
        {this.state.imageStatus == 'loading' && this.renderSpinner() }
        {this.state.imageStatus == 'failed' && this.renderFallback() }
      </div>
    );
  }
}

ImageSpin.defaultProps = {
//   loaderStyle: 'gear',  //example
  image: defaultImage,
};

ImageSpin.propTypes = {
  image: PropTypes.string.isRequired,
  loaderStyle: PropTypes.string,
  alt: PropTypes.string,
};

export default ImageSpin;
