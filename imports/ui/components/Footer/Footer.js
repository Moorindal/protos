import React from 'react';
import { Link } from 'react-router-dom';
import { Grid } from 'react-bootstrap';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { year } from '../../../modules/dates';
import PopularPosts from '../PopularPosts/PopularPosts';

// const StyledFooter = styled.div`
//   position: absolute;
//   bottom: 0;
//   width: 100%;
//   height: 60px;
//   background-color: #fff;
//   border-top: 1px solid var(--gray-lighter);
//   padding: 20px 0;
// 
//   p {
//     color: var(--gray-light);
//     font-size: 14px;
//   }
// 
//   ul {
//     list-style: none;
//     padding: 0;
//   }
// 
//   ul li {
//     float: left;
// 
//     &:first-child {
//       margin-right: 15px;
//     }
// 
//     a {
//       color: var(--gray-light);
//     }
// 
//     a:hover,
//     a:active,
//     a:focus {
//       text-decoration: none;
//       color: var(--gray);
//     }
//   }
// 
//   @media screen and (min-width: 768px) {
//     ul li:first-child {
//       margin-right: 30px;
//     }
//   }
// `;

const { productName, copyrightStartYear } = Meteor.settings.public;
const copyrightYear = () => {
  const currentYear = year();
  return currentYear === copyrightStartYear ? copyrightStartYear : `${copyrightStartYear}-${currentYear}`;
};

// const Footer = () => (
//   <StyledFooter>
//     <Grid>
//       <p className="pull-left">&copy; {copyrightYear()} {productName}</p>
//       <ul className="pull-right">
//         <li><Link to="/terms">Terms<span className="hidden-xs"> of Service</span></Link></li>
//         <li><Link to="/privacy">Privacy<span className="hidden-xs"> Policy</span></Link></li>
//       </ul>
//     </Grid>
//   </StyledFooter>
// );

const Footer = () => (
    <footer className="footer_area">
        <div className="footer_row row">
            <div className="col l3 m6 footer_col">
              <PopularPosts />
            </div> 
            <div className="col l3 m6 footer_col footer_trending"> 
                <h3 className="categories_tittle">Trending</h3> 
                <div className="trending_area">
                    <ul className="collapsible trending_collaps" data-collapsible="accordion">
                        <li>
                            <div className="collapsible-header"><i className="ion-chevron-right"></i>Healthy Environment For Self Esteem</div>
                            <div className="collapsible-body">
                                <div className="row collaps_wrpper"> 
                                    <div className="col s1 media_l">
                                        <b>1</b>
                                        <i className="icon ion-md-arrow-dropup-circle"></i>
                                    </div>
                                    <div className="col s11 media_b">
                                        <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                        <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem</p>
                                        <h6>By <a href="#">Thomas Omalley</a></h6>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li>
                            <div className="collapsible-header"><i className="ion-chevron-right"></i>Burn The Ships</div>
                            <div className="collapsible-body">
                                <div className="row collaps_wrpper"> 
                                    <div className="col s1 media_l">
                                        <b>1</b>
                                        <i className="icon ion-md-arrow-dropup-circle"></i>
                                    </div>
                                    <div className="col s11 media_b">
                                        <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                        <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem</p>
                                        <h6>By <a href="#">Thomas Omalley</a></h6>
                                    </div>
                                </div> 
                            </div>
                        </li>
                        <li>
                            <div className="collapsible-header active"><i className="ion-chevron-right"></i>Harness The Power Of Your Dreams</div>
                            <div className="collapsible-body">
                                <div className="row collaps_wrpper"> 
                                    <div className="col s1 media_l">
                                        <b>1</b>
                                        <i className="icon ion-md-arrow-dropup-circle"></i>
                                    </div>
                                    <div className="col s11 media_b">
                                        <a href="#" className="close_btn"><i className="ion-close-round"></i></a>
                                        <p>If you will be traveling for a ski vacation, it is often difficult to know what to pack. You may not even have a problem</p>
                                        <h6>By <a href="#">Thomas Omalley</a></h6>
                                    </div>
                                </div> 
                            </div>
                        </li> 
                    </ul> 
                </div>
            </div> 
            <div className="col l3 m6 footer_col">
                <div className="badges">
                    <h3 className="categories_tittle">Badges</h3> 
                    <ul className="badges_list">
                        <li><a href="#"><i className="icon ion-md-bonfire"></i><span>6</span></a></li>
                        <li><a href="#"><i className="icon ion-md-bluetooth"></i></a></li>
                        <li><a href="#"><i className="icon ion-md-clock"></i> <span>3</span></a></li>
                        <li><a href="#"><i className="icon ion-md-camera"></i></a></li>
                        <li><a href="#"><i className="icon ion-md-bluetooth"></i></a></li>
                        <li><a href="#"><i className="icon ion-md-heart"></i></a></li>
                    </ul>
                </div>
                
                <div className="social_Sharing">
                    <h3 className="categories_tittle">Social Sharing</h3>  
                    <ul className="social_icon">
                        <li><a href="#"><i className="icon ion-logo-twitter"></i></a></li>
                        <li><a href="#" className="tumblr"><i className="icon ion-logo-tumblr"></i></a></li>
                        <li><a href="#" className="googleplus"><i className="icon ion-logo-googleplus"></i></a></li>
                        <li><a href="#" className="pinterest"><i className="icon ion-logo-pinterest"></i></a></li>
                        <li><a href="#" className="facebook"><i className="icon ion-logo-facebook"></i></a></li>
                    </ul>
                </div>
            </div> 
            <div className="col l3 m6 footer_col">
                <img src="/placeholders/advertis-3.jpg" alt="" className="responsive-img" />
            </div> 
        </div>
        <div className="copy_right">
            &copy; {copyrightYear()} {productName} All rights reserved.
        </div>
      </footer>
);

Footer.propTypes = {};

export default Footer;
