/* eslint-disable consistent-return */

import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check, Match } from 'meteor/check';
import LinkedAccounts from './LinkedAccounts';
import handleMethodException from '../../modules/handle-method-exception';
import rateLimit from '../../modules/rate-limit';

Meteor.methods({
  'linkedAccounts.findOne': function linkedAccountsFindOne(linkedAccountId) {
    console.log('linkedAccounts.findOne ', linkedAccountId);
    check(linkedAccountId, Match.OneOf(String, undefined));

    try {
      var o_id = new Mongo.ObjectID(linkedAccountId);     //only native ids
  
      let result = LinkedAccounts.findOne({'_id': o_id});
      console.dir(result);
      return result;
    } catch (exception) {
      handleMethodException(exception);
    }
  },
  'linkedAccounts.insert': function linkedAccountsInsert(acc) {
    console.log('called linkedAccounts.insert');
    check(acc, {
      owner: String,
      username: String,
      network: String,
      data: Object,
    });

    try {
      return LinkedAccounts.insert({ owner: this.userId,
                                     network: acc.network,
                                     username: acc.username,
                                     data: acc.data,
                                     ...acc
      });
    } catch (exception) {
      handleMethodException(exception);
    }
  },
  'linkedAccounts.update': function linkedAccountsUpdate(acc) {
    console.log('called linkedAccounts.update');
    check(acc, {
      _id: String,
      owner: String,
      username: String,
      network: String,
      data: Object,
    });

    try {
      const linkedAccountId = acc._id;
      const accToUpdate = LinkedAccounts.findOne(linkedAccountId, { fields: { owner: 1 } });

      if (accToUpdate.owner === this.userId) {
        LinkedAccounts.update(linkedAccountId, { $set: acc });
        return linkedAccountId; // Return _id so we can redirect to linkedAccount after update.
      }

      throw new Meteor.Error('403', 'You\'re not allowed to edit this linkedAccount.');
    } catch (exception) {
      handleMethodException(exception);
    }
  },
  'linkedAccounts.remove': function linkedAccountsRemove(linkedAccountId) {
    check(linkedAccountId, String);

    try {
      const accToRemove = LinkedAccounts.findOne(linkedAccountId, { fields: { owner: 1 } });

      if (accToRemove.owner === this.userId) {
        return LinkedAccounts.remove(linkedAccountId);
      }

      throw new Meteor.Error('403', 'You\'re not allowed to delete this linkedAccount.');
    } catch (exception) {
      handleMethodException(exception);
    }
  },
  'linkedAccounts.connectSteem': function linkedAccountsConnectSteem(account) {
    console.log('called linkedAccounts.connectSteem');
    check(account, {
      owner: String,
      username: String,
      network: String,
      data: Object,
    });
    
    try {
      let accountExists = LinkedAccounts.findOne({'owner': account.owner,
                                                  'network': account.network,
                                                 });
      
      if (!accountExists) {
        console.log('Account not found, creating.');
        return LinkedAccounts.insert({ owner: this.userId,
                                      network: account.network,
                                      username: account.username,
                                      data: account.data,
                                      ...account
        });
      } else {
        console.log('Account found, updating.');
        console.dir(accountExists);
        return LinkedAccounts.update(accountExists._id, { $set: account });
      }
    } catch (exception) {
      handleMethodException(exception);
    }

  },
});

rateLimit({
  methods: [
    'linkedAccounts.insert',
    'linkedAccounts.update',
    'linkedAccounts.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
