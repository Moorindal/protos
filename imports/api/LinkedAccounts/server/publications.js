import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import LinkedAccounts from '../LinkedAccounts';

console.log('Run publications for linkedAccounts');

Meteor.publish('linkedAccounts', function linkedAccounts() {
  console.log('Published linkedAccounts');
  return LinkedAccounts.find({ owner: this.userId });
});

// Note: linkedAccounts.view is also used when editing an existing document.
Meteor.publish('linkedAccounts.view', (linkedAccountId) => {
  check(linkedAccountId, String);
  return LinkedAccounts.find({ _id: linkedAccountId });
});

Meteor.publish('linkedAccounts.edit', function linkedAccountsEdit(linkedAccountId) {
  check(linkedAccountId, String);
  return LinkedAccounts.find({ _id: linkedAccountId, owner: this.userId });
});
