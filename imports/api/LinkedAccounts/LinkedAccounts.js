/* eslint-disable consistent-return */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const LinkedAccounts = new Mongo.Collection('LinkedAccounts');

LinkedAccounts.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

LinkedAccounts.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

LinkedAccounts.schema = new SimpleSchema({
  owner: {
    type: String,
    label: 'The ID of the user this account belongs to.',
  },
  createdAt: {
    type: String,
    label: 'The date this account was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    label: 'The date this account was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  username: {
    type: String,
    label: 'Linked account name',
  },
  network: {
    type: String,
    label: 'The network of the account (i.e. Steem, Whaleshares, etc.).',
  },
  data: {
    type: Object,
    label: 'Auth data.',
    optional: true,
    blackbox: true,
  },
});

LinkedAccounts.attachSchema(LinkedAccounts.schema);

export default LinkedAccounts;
