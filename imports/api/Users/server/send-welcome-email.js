import { Meteor } from 'meteor/meteor';
import sendEmail from '../../../modules/server/send-email';
import getOAuthProfile from '../../../modules/get-oauth-profile';

export default (oauthUser) => {
  const user = oauthUser || Meteor.user();
  const OAuthProfile = getOAuthProfile(user.profile, user);
  const { productName } = Meteor.settings.public;
  const { supportEmail } = Meteor.settings.private;
  const username = OAuthProfile ? OAuthProfile.username : user.profile.username;
  const emailAddress = OAuthProfile ? OAuthProfile.email : user.emails[0].address;

  return sendEmail({
    to: emailAddress,
    from: supportEmail,
    subject: `[${productName}] Welcome, ${username}!`,
    template: 'welcome',
    templateVars: {
      title: `Welcome, ${username}!`,
      subtitle: `Here's how to get started with ${productName}.`,
      productName,
      username,
      welcomeUrl: Meteor.absoluteUrl('documents'), // e.g., returns http://localhost:3000/documents
    },
  })
    .catch((error) => {
      throw new Meteor.Error('500', `${error}`);
    });
};
