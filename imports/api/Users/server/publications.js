import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

Meteor.publish('users.editProfile', function usersProfile() {
  return Meteor.users.find(this.userId, {
    fields: {
      emails: 1,
      profile: 1,
      services: 1,
      accounts: 1,
    },
  });
});


Meteor.publish('users.getUser', function (username) {
  console.log('users.getUser for ', username)
  check(username, String);
  return Meteor.users.find({'profile.username': username}, {
    fields: {
      profile: 1,
      roles: 1,
      accounts: 1,
    }
  });
});
