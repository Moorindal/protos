/* eslint-disable consistent-return */

import _ from 'lodash';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

let action;

const connectSteem = ({ userId, accounts }, promise) => {
  console.log('/*connectSteem*/');
  console.dir(accounts);
  
  try {
    action = promise;
    Meteor.users.update(userId, {
      $set: {
        accounts,
      },
    });
    action.resolve();
  } catch (exception) {
    action.reject(exception.message);
  }
};

export default options =>
  new Promise((resolve, reject) =>
    connectSteem(options, { resolve, reject }));
